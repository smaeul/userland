/* $NetBSD: sleep.c,v 1.29 2019/01/27 02:00:45 christos Exp $ */

/*
 * Copyright (c) 1988, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <ctype.h>
#include <err.h>
#include <locale.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <bsd/stdlib.h>
#include <time.h>
#include <unistd.h>

static void alarmhandle(int);
static void usage(void);

static void report(const time_t, const time_t, const char *const);

static volatile sig_atomic_t report_requested;
static void
report_request(int signo)
{

	report_requested = 1;
}

int
main(int argc, char *argv[])
{
	char *arg, *temp;
	const char *msg;
	double fval, ival, val;
	struct timespec ntime;
	time_t original;
	int ch;
	unsigned delay;

	setprogname(argv[0]);
	(void)setlocale(LC_ALL, "");

	(void)signal(SIGALRM, alarmhandle);

	while ((ch = getopt(argc, argv, "")) != -1)
		switch(ch) {
		default:
			usage();
		}
	argc -= optind;
	argv += optind;

	if (argc != 1)
		usage();

	arg = *argv;
	for (temp = arg; *temp != '\0'; temp++)
		if (!isdigit((unsigned char)*temp)) {
			usage();
		}

	ntime.tv_sec = strtol(arg, &temp, 10);
	if (ntime.tv_sec < 0 || temp == arg || *temp != '\0')
		usage();

	if (ntime.tv_sec == 0)
		return EXIT_SUCCESS;
	ntime.tv_nsec = 0;

	original = ntime.tv_sec;
	if (ntime.tv_nsec != 0)
		msg = " and a bit";
	else
		msg = "";

	signal(SIGHUP, report_request);

	if (ntime.tv_sec <= 10000) {			/* arbitrary */
		while (nanosleep(&ntime, &ntime) != 0) {
			if (report_requested) {
				report(ntime.tv_sec, original, msg);
				report_requested = 0;
			} else
				err(EXIT_FAILURE, "nanosleep failed");
		}
	} else while (ntime.tv_sec > 0) {
		delay = (unsigned int)ntime.tv_sec;

		if ((time_t)delay != ntime.tv_sec || delay > 30 * 86400)
			delay = 30 * 86400;

		ntime.tv_sec -= delay;
		delay = sleep(delay);
		ntime.tv_sec += delay;

		if (delay != 0 && report_requested) {
			report(ntime.tv_sec, original, "");
			report_requested = 0;
		} else
			break;
	}

	return EXIT_SUCCESS;
	/* NOTREACHED */
}

	/* Reporting does not bother with nanoseconds. */
static void
report(const time_t remain, const time_t original, const char * const msg)
{
	if (remain == 0)
		warnx("In the final moments of the original"
		    " %jd%s second%s", (intmax_t)original, msg,
		    original == 1 && *msg == '\0' ? "" : "s");
	else if (remain < 2000)
		warnx("Between %jd and %jd seconds left"
		    " out of the original %g%s",
		    (intmax_t)remain, (intmax_t)remain + 1, (double)original,
		    msg);
	else if ((original - remain) < 100000 && (original-remain) < original/8)
		warnx("Have waited only %jd seconds of the original %g",
			(intmax_t)(original - remain), (double)original);
	else
		warnx("Approximately %g seconds left out of the original %g",
			(double)remain, (double)original);
}

static void
usage(void)
{
	(void)fprintf(stderr, "usage: %s seconds\n", getprogname());
	exit(EXIT_FAILURE);
	/* NOTREACHED */
}

/* ARGSUSED */
static void
alarmhandle(int i)
{
	_exit(EXIT_SUCCESS);
	/* NOTREACHED */
}
